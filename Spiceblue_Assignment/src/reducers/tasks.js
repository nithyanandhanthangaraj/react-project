export default (state={}, action)=>{
    switch (action.type) {
        case 'TASKS_LIST':
            return { ...state, tasksList: action.data}
        case 'TASK_DETAILS':
            return { ...state, taskDetails: action.data}
        case 'RESET_TASKS':
                return {}
        default:
            return state;
    }
}