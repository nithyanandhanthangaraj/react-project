export default (state={}, action)=>{
    switch (action.type) {
        case 'ACCESS_TOKEN':
            return { ...state, token: action.data};
        case 'USER_DETAILS':
            return { ...state, user: action.data}
        default:
            return state;
    }
}