export default (state={}, action)=>{
    switch (action.type) {
        case 'IS_LOADING':
            return { ...state, Loading: action.data};
        default:
            return state;
    }
}