import auth from './auth';
import tasks from './tasks';
import loader from './loader';

export default {
    auth,
    tasks,
    loader
}