// import logo from './logo.svg';
import './App.css';
import Login from './Login';
import { useDispatch, useSelector } from 'react-redux';
// import Home from './Home';
import { Route, Switch } from 'react-router-dom';
import Logout from './Logout';
import TaskPage from './TaskPage';

const App = ()=> {
  const { token } = useSelector(state=> state.auth);
  return (
    <div className="App">
      {token && <Logout /> }
      <header className="App-header">
        {/* <Login/> */}
        {/* <img src={logo} className="App-logo" alt="logo" /> */}
        <Switch>
            <Route exact path = "/" render={props => (token ? <TaskPage /> : <Login />)} />
        </Switch>
      </header>
    </div>
  );
}

export default App;
