import Axios from 'axios';

export const getHttpMethods = () => {
    var http={};
    Axios.defaults.headers.common['Accept'] = 'application/json';
    Axios.defaults.headers.common['Content-Type'] = 'application/json';
 
    http.get = function (uri, input) {
        return Axios.get(uri, input);
    }

    http.post = function (uri, input) {
        return new Promise(function (resolve, reject){
            try {
                Axios.post(uri, input)
                .then((response)=>{
                    resolve(response);
                })
                .catch((error)=>{
                    reject(error);
                })
            } catch (error) {
                reject(error);
            }
        })
    }
    return http;
}