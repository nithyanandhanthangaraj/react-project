import React, { useEffect, useState } from 'react';
import {
    Container,
    Row,
    Alert
} from 'reactstrap';
import { useDispatch } from 'react-redux';
import { getHttpMethods } from './httpMethods';
import Axios from 'axios';

const Login = () => {
    const dispatch = useDispatch(),
        [login, setLogin] = useState(false),
    appLogin = ()=>{
        setLogin(true);
    };

    useEffect(() => {
        if (login) {
            let body = {
                email: 'smithcheryl@yahoo.com',
                password: '12345678'
            }
            const http = getHttpMethods();
            http.post(`https://stage.api.sloovi.com/login`, body)
                .then((res) => {
                    let token = res.data && res.data.results && res.data.results.token;
                    dispatch({type:'ACCESS_TOKEN', data: token})
                    setLogin(false);
                    if(token) {
                        Axios.defaults.headers.common['Accept'] = 'application/json';
                        Axios.defaults.headers.common['Content-Type'] = 'application/json';
                        Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
                        Axios.get(`https://stage.api.sloovi.com/user`)
                        .then((res) => {
                            dispatch({type:'USER_DETAILS', data: res.data.results})
                        })
                        .catch((err) => {
                            console.log("ERROR :::", err);
                        }) 
                    }
                })
                .catch((err) => {
                    console.log("ERROR :::", err);
                })
        }
    })

    return (
        <>
            <Container style={{width: '50%', paddingTop: '15%'}}>
                <Alert color="dark">
                    <Container>
                        <Row>
                            <h3 style= {{color:'#3399ff'}}>
                            <button color="buttonAsLink" style={{width:'100%'}} onClick={()=>appLogin()}>
                                Login
                            </button>
                            </h3>
                            
                        </Row>
                    </Container>
                </Alert>
            </Container>
        </>
    )
}

export default Login;