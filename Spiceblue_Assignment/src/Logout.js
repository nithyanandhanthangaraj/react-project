import React from 'react';
import {
    Button,
    Container,
    Row,
    Col
} from 'reactstrap';
import { useDispatch } from 'react-redux';

const Logout = () => {
    const dispatch = useDispatch(),
        appLogout = () => {
            dispatch({ type: 'ACCESS_TOKEN', data: undefined });
            dispatch({ type: 'RESET_TASKS' });
            dispatch({ type: 'USER_DETAILS', data: undefined });
        };
    return (
        <>
            <Container style={{  paddingTop: '2%' }}>
                    <Row>
                        <Col sm={2}>
                        <Button onClick={() => appLogout()}>
                            Logout
                        </Button>
                        </Col>
                    </Row>
            </Container>
        </>
    )
}

export default Logout;