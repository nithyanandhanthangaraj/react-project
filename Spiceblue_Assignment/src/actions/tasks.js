
import Axios from 'axios';
import moment from 'moment';

export const getTasksList = () => {
    return (dispatch, state) => {
        let store = state();

        dispatch({ type: "IS_LOADING", data: true })
        let token = store.auth.token;
        let url = `https://stage.api.sloovi.com/task/lead_58be137bfde045e7a0c8d107783c4598`

        Axios.defaults.headers.common['Accept'] = 'application/json';
        Axios.defaults.headers.common['Content-Type'] = 'application/json';
        Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        Axios.get(url)
            .then((res) => {
                dispatch({ type: 'TASKS_LIST', data: res.data.results })
            })
            .then(() => {
                dispatch({ type: "IS_LOADING", data: false })
            })
            .catch((err) => {
                console.log("ERROR-->", err);
                dispatch({ type: "IS_LOADING", data: false })
            })
    }
}

export const createTask = (task) => {
    return (dispatch, state) => {
        let store = state();
        console.log("createdata:::", task, task.taskDate, typeof task.taskDate, "date moment::",task.taskTime && moment(task.taskTime),task.taskTime && moment(task.taskTime).toDate(), (new Date(task.taskTime).getTime)/1000);
       
       let dattime =task.taskTime && moment(task.taskTime).toDate();
       console.log("dattime--->", dattime, dattime.getTime()/1000)
        dispatch({ type: "IS_LOADING", data: true })
        let token = store.auth.token;
        let user = store.auth.user;
        let url = `https://stage.api.sloovi.com/task/lead_58be137bfde045e7a0c8d107783c4598`

        let body = {
            assigned_user: user.id,
            task_date: formatDateMoment(task.taskDate), //formatDate(task.taskDate),
            task_time: parseInt(dattime.getTime()/1000),//12346567,//Date.parse(task.taskTime),
            is_completed: 1,
            time_zone: timezone_offset_in_seconds(new Date()),
            task_msg: task.taskDesc
        }
      
        console.log("body1--->", body);
        // let body = {
        //     assigned_user: user.id,
        //     task_date: '2020-12-26',
        //     formatDate: formatDateMoment(task.taskDate),
        //     task_time: Date.parse(task.taskTime),
        //     is_completed: 1,
        //     time_zone: 12345,
        //     task_msg: task.taskDesc
        // }
        // console.log("BODY--->", body);
        Axios.defaults.headers.common['Accept'] = 'application/json';
        Axios.defaults.headers.common['Content-Type'] = 'application/json';
        Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        Axios.post(url, body)
            .then((res) => {
                console.log("Adding task--->", res);
                dispatch(getTasksList())
            })
            .then(() => {
                dispatch({ type: "IS_LOADING", data: false })
            })
            .catch((err) => {
                dispatch({ type: "IS_LOADING", data: false })
                console.log("ERROR-->", err);
            })
    }
}
export const updateTask = (task) => {
    return (dispatch, state) => {
        let store = state();

        dispatch({ type: "IS_LOADING", data: true })
        let token = store.auth.token;
        let user = store.auth.user;
        let url = `https://stage.api.sloovi.com/task/lead_58be137bfde045e7a0c8d107783c4598/${task.id}`
        console.log("TASKKK--->", task);
        let dattime =task.taskTime && moment(task.taskTime).toDate();

        let body = {
            assigned_user: user.id,
            task_date: formatDateMoment(task.taskDate),
            task_time: parseInt(dattime.getTime()/1000),
            is_completed: 1,
            time_zone: timezone_offset_in_seconds(new Date()),
            task_msg: task.taskDesc
        }
        console.log("body1 in update--->", body);
       
        Axios.defaults.headers.common['Accept'] = 'application/json';
        Axios.defaults.headers.common['Content-Type'] = 'application/json';
        Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        Axios.put(url, body)
            .then((res) => {
                console.log("Adding task--->", res);
                dispatch(getTasksList())
            })
            .then(() => {
                dispatch({ type: "IS_LOADING", data: false })
            })
            .catch((err) => {
                dispatch({ type: "IS_LOADING", data: false })
                console.log("ERROR-->", err);
            })
    }
}
const formatDateMoment = (date)=>{
    return date && moment(date).format("YYYY-MM-DD");
}
const timezone_offset_in_seconds = (dt) => {
    return -dt.getTimezoneOffset() * 60;
}

const formatDate = (date) => {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}


export const getTaskDetails = (task) => {
    return (dispatch, state) => {
        let store = state();

        dispatch({ type: "IS_LOADING", data: true })
        let token = store.auth.token;
        let url = `https://stage.api.sloovi.com/lead_58be137bfde045e7a0c8d107783c4598/${task.id}`

        Axios.defaults.headers.common['Accept'] = 'application/json';
        Axios.defaults.headers.common['Content-Type'] = 'application/json';
        Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        Axios.get(url)
            .then((res) => {
                console.log("Adding task--->", res);
                let data = res.data.results;
                let frameRes = {
                    id: task.id,
                    taskDesc: task.task_msg,
                    taskDate: new Date(task.task_date),
                    taskTime: new Date(task.task_time),
                    username: task.user_name
                }
                dispatch({ type: 'TASK_DETAILS', data: frameRes })
            })
            .then(() => {
                dispatch({ type: "IS_LOADING", data: false })
            })
            .catch((err) => {
                dispatch({ type: "IS_LOADING", data: false })
                console.log("ERROR-->", err);
            })
    }
}

export const deleteTask = (task) => {
    return (dispatch, state) => {
        let store = state();

        dispatch({ type: "IS_LOADING", data: true })
        let token = store.auth.token;
        let url = `https://stage.api.sloovi.com/task/lead_58be137bfde045e7a0c8d107783c4598/${task}`

        Axios.defaults.headers.common['Accept'] = 'application/json';
        Axios.defaults.headers.common['Content-Type'] = 'application/json';
        Axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        Axios.delete(url)
            .then((res) => {
                console.log("Deleting task--->", res);    
                dispatch({type:"TASK_DETAILS", data: undefined})
                dispatch(getTasksList());          
            })
            .then(() => {
                dispatch({ type: "IS_LOADING", data: false })
            })
            .catch((err) => {
                dispatch({ type: "IS_LOADING", data: false })
                console.log("ERROR-->", err);
            })
    }
}
