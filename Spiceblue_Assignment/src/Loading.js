import { Spinner } from "reactstrap";

const Loading =()=>(
    <Spinner type="grow" style={{color:'black'}} />
)
export default Loading;