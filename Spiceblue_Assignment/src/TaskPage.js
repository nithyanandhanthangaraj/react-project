import { connect } from 'react-redux';
import React from 'react';
import * as tasksActions from './actions/tasks';
import Loading from './Loading';
import {
    Button,
    Container,
    Row,
    Col,
    Form,
    Input,
    Label,
    FormGroup
} from 'reactstrap';
import { AiOutlinePlus, AiOutlineRedEnvelope, AiFillEdit, AiOutlineClockCircle, AiOutlineCalendar } from 'react-icons/ai';
import DatePicker from 'react-datepicker';
import { BsTrashFill } from 'react-icons/bs';
import moment from 'moment';

class TaskPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: null,
            collapse: false,
            taskDesc: '',
            taskDate: null,
            taskTime: null
        }
    }

    componentDidMount() {
        this.props.getTasksList();
    }
    togglePanel = () => {
        this.setState({ collapse: !this.state.collapse })
        this.setState({
            taskDesc: '',
            taskDate: null,
            taskTime: null
        })
    }
    formatDateMoment = (date) => {
        return date && moment(date).format("YYYY-MM-DD");
    }
    handleSubmit(e) {
        e.preventDefault();
        const { taskDesc, taskDate, taskTime } = this.state;
        let taskData = {
            taskDate,
            taskDesc,
            taskTime
        }
        if (this.props.taskDetails) {
            this.setState({ collapse: false })
            this.props.updateTask(taskData = { ...taskData, id: this.props.taskDetails.id })
        }
        else {
            this.setState({ collapse: false })
            this.props.createTask(taskData);
        }
    }
    getTaskDetail(task) {
        if (this.state.edit || !this.props.taskDetails) {
            this.props.getTaskDetails(task);
            this.setState({
                edit: false,
                id: task.id,
                taskDesc: task.task_msg,
                taskDate: new Date(task.task_date),
                taskTime: new Date(task.task_time * 1000),
                username: task.user_name
            })
        }
    }
    render() {
        const { tasksList, user } = this.props;
        const { taskDesc, taskDate, taskTime, collapse } = this.state;
        let userName = user && user.name;
        return (
            <>
                <Container style={{ width: '50%', paddingTop: '2%' }}>
                    {this.props.Loading && <Loading />}
                    {tasksList && (
                        <Form style={{ width: '50%' }}>
                            <Row>
                                <Col sm={12}>
                                    <Input
                                        type='text'
                                        disabled
                                        value={`TASKS   ${tasksList.length}`}
                                    />
                                </Col>
                                <Col>
                                    <AiOutlinePlus style={{ float: 'right', color: 'black', marginTop: '-28px', cursor: "pointer" }} onClick={e => this.togglePanel(e)} />
                                </Col>
                            </Row>
                            {collapse && (
                                <>
                                    <Container style={{ backgroundColor: '#d1efed', position: 'absolute', width: '24%' }}>
                                        <Row>
                                            <Col sm={12}>
                                                <FormGroup>
                                                    <Label className='label' style={{ float: 'left', color: 'black' }}>Task Description</Label>
                                                    <div>
                                                        <>
                                                            <Input type="text" value={taskDesc}
                                                                onChange={event => {
                                                                    this.setState({ taskDesc: event.target.value })
                                                                }}>
                                                            </Input>
                                                            <AiOutlineRedEnvelope
                                                                style={{
                                                                    marginTop: '-30px', color: 'black', float: 'right'
                                                                }}
                                                            />
                                                        </>
                                                    </div>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row style={{ marginTop: '10px' }}>
                                            <Col sm={6}>
                                                <FormGroup>
                                                    <Label className='label'>Date</Label>
                                                    <div>
                                                        <DatePicker
                                                            className="datePicker"
                                                            name="taskDate"
                                                            dateFormat="MM/dd/yyyy"
                                                            selected={taskDate}
                                                            placeholderText="Date"
                                                            onChange={val => {
                                                                this.setState({ taskDate: val })
                                                            }}
                                                        />
                                                        <AiOutlineCalendar
                                                            style={{
                                                                marginTop: '-23px',
                                                                color: 'black',
                                                                float: 'right',
                                                                position: 'relative'
                                                            }} />
                                                    </div>
                                                </FormGroup>
                                            </Col>
                                            <Col sm={6}>
                                                <FormGroup>
                                                    <Label className='label'>Time</Label>
                                                    <div>
                                                        <DatePicker className="datePicker" showTimeSelect showTimeSelectOnly dateFormat="HH:mm" timeIntervals={30}
                                                            placeholderText="Time"
                                                            selected={taskTime}
                                                            onChange={val => {
                                                                this.setState({ taskTime: val })
                                                            }}
                                                        />
                                                        <AiOutlineClockCircle style={{
                                                            marginTop: '-23px',
                                                            color: 'black',
                                                            float: 'right',
                                                            position: 'relative'
                                                        }} />
                                                    </div>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={12}>
                                                <FormGroup>
                                                    <Label className='label'>Assign User</Label>
                                                    <Input type="text" name="task" disabled value={userName}></Input>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row style={{ marginTop: '10px' }}>
                                            <div style={{ float: 'right' }}>
                                                {this.props.taskDetails &&
                                                    <BsTrashFill style={{ color: 'black', float: 'left', marginTop: '10px', cursor: 'pointer' }} onClick={(e) => {
                                                        if (window.confirm("Are you sure you want to delete the task?")) {
                                                            this.setState({ collapse: false }, () => this.props.deleteTask(this.props.taskDetails.id))

                                                        }
                                                    }} />
                                                }
                                                <span
                                                    style={{ float: 'right', color: 'black', fontSize: 'medium', cursor: 'pointer' }}
                                                    onClick={() => {
                                                        this.setState({ collapse: false });
                                                        this.props.resetTaskDetails();
                                                    }}
                                                >
                                                    Cancel
                                                            <Button type='submit'
                                                        style={{
                                                            marginLeft: '2px',
                                                            backgroundColor: 'mediumseagreen'
                                                        }}
                                                        onClick={(e) => this.handleSubmit(e)}>Save</Button>
                                                </span>
                                            </div>
                                        </Row>
                                    </Container>
                                </>
                            )}
                            {tasksList.map((item, i) => (
                                <div key={i}>
                                    <Row style={{ border: 'groove' }}>

                                        <div style={{ color: 'black', fontSize: 'medium' }}>
                                            <span style={{ float: 'left' }}>{item.task_msg}</span>
                                            <AiFillEdit
                                                onClick={() => {
                                                    this.setState({ collapse: true, edit: true }, () => this.getTaskDetail(item))
                                                }}
                                                style={{
                                                    float: 'right',
                                                    fontSize: 'medium',
                                                    color: 'black',
                                                    cursor: "pointer"
                                                }}
                                            />
                                        </div>
                                        <div style={{ color: 'black', fontSize: 'medium' }}>
                                            <span style={{ float: 'left' }}>{this.formatDateMoment(item.task_date)}</span>
                                        </div>
                                    </Row>
                                </div>
                            ))}
                        </Form>
                    )}
                </Container>
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        tasksList: state.tasks.tasksList,
        Loading: state.loader.Loading,
        user: state.auth.user,
        taskDetails: state.tasks.taskDetails
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getTasksList: () => dispatch(tasksActions.getTasksList()),
        createTask: (...props) => dispatch(tasksActions.createTask(...props)),
        updateTask: (...props) => dispatch(tasksActions.updateTask(...props)),
        getTaskDetails: (...props) => dispatch(tasksActions.getTaskDetails(...props)),
        deleteTask: (...props) => dispatch(tasksActions.deleteTask(...props)),
        resetTaskDetails: () => dispatch({ type: "TASK_DETAILS", data: undefined })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskPage);
